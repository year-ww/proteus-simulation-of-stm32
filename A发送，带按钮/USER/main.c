#include "led.h"
#include "delay.h"
#include "sys.h"
#include "key.h"
#include "usart.h"
#include "exti.h" 

void Delays1(uint16_t xms)
 {
	uint16_t i,j;
	 for(i=xms;i>0;i--)
		for(j=110;j>0;j--);
 }
 
 u16 a ;
 int main(void)
 {	
	delay_init();	    	 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(9600);
	EXTIX_Init();
	LED_Init();		  	 
	while(1)
	{
        
        Delays1(1);
				a ++ ;
		if(a == 500)
		{
			a = 0 ;
			LED0=!LED0;
		}
	}	 
}
