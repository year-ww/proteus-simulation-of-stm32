#include "lcd.h"
#include "delay.h"	
#include "sys.h"



//extern OS_FLAG_GRP	*EventFlags;		//定义一个事件标志组
#define KEY_FLAG		0x01

void LCD_show_number1()
{
		vLCDInitialize();
		//清屏
		printWidthX24(0, 127, 127, data_clear);
		printWidthX24(0, 0, 128, data_clear);
		
		printWidthX24(3, 127, 127, data_clear);
		printWidthX24(3, 0, 128, data_clear);
		
		printWidthX16(6, 0, 128, data_clear);
		
		//打出“电压   V”
		
//		printWidthX24(0, 67, 16, data_huang);
//		printWidthX24(0, 82, 16, data_wang);
	//	printWidthX24(0, 96, 16, data_bo);
		
		
		printWidthX24(2, 67, 8, data_2);
		printWidthX24(2, 75, 8, data_1);
		printWidthX24(2, 83, 8, data_0);
		printWidthX24(2, 91, 8, data_0);
		
		printWidthX24(4, 67, 8, data_4);
		printWidthX24(4, 75, 8, data_6);
		printWidthX24(4, 83, 8, data_0);
		printWidthX24(4, 91, 8, data_4);
		printWidthX24(4, 99, 8, data_1);
		printWidthX24(4, 107, 8, data_2);	
}
void LCD_show_number2()
{
//		vLCDInitialize();
//		//清屏
		printWidthX24(0, 127, 127, data_clear);
		printWidthX24(0, 0, 128, data_clear);
		
		printWidthX24(3, 127, 127, data_clear);
		printWidthX24(3, 0, 128, data_clear);
		
		printWidthX16(6, 0, 128, data_clear);
//		
//		printWidthX24(0, 67, 16, data_huang);
//		printWidthX24(0, 82, 16, data_wang);
		
		
		printWidthX24(2, 67, 8, data_2);
		printWidthX24(2, 75, 8, data_1);
		printWidthX24(2, 83, 8, data_0);
		printWidthX24(2, 91, 8, data_0);
		
		printWidthX24(4, 67, 8, data_4);
		printWidthX24(4, 75, 8, data_6);
		printWidthX24(4, 83, 8, data_0);
		printWidthX24(4, 91, 8, data_4);
		printWidthX24(4, 99, 8, data_1);
		printWidthX24(4,107, 8, data_2);	
}


void LCD_Init(void)
{
	//端口配置
 GPIO_InitTypeDef  LCD_InitStructure; 	
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	 //使能PC端口时钟	
 LCD_InitStructure.GPIO_Pin = D7_Pin|RST_Pin|CS1_Pin|CS2_Pin 
                          |EN_Pin|RW_Pin|RS_Pin|D0_Pin 
                          |D1_Pin|D2_Pin|D3_Pin|D4_Pin 
                          |D5_Pin|D6_Pin;
 LCD_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 LCD_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOC, &LCD_InitStructure);					     //根据设定参数初始化GPIO
 GPIO_WriteBit(GPIOC, D7_Pin|RST_Pin|CS1_Pin|CS2_Pin 
                          |EN_Pin|RW_Pin|RS_Pin|D0_Pin 
                          |D1_Pin|D2_Pin|D3_Pin|D4_Pin 
                          |D5_Pin|D6_Pin,Bit_RESET); //LCD18个位全部拉低

	
	
	//00000000
	
//	vLCDInitialize();
//	//清屏
//	printWidthX24(0, 127, 127, data_clear);
//	printWidthX24(0, 0, 128, data_clear);
//	
//	printWidthX24(3, 127, 127, data_clear);
//	printWidthX24(3, 0, 128, data_clear);
//	
//	printWidthX16(6, 0, 128, data_clear);
//	
//	//打出“电压   V”
//	
//	printWidthX24(0, 67, 16, data_huang);
//	printWidthX24(0, 82, 16, data_wang);
////	printWidthX24(0, 96, 16, data_bo);
//	
//	
//	printWidthX24(2, 67, 8, data_2);
//	printWidthX24(2, 75, 8, data_1);
//	printWidthX24(2, 83, 8, data_0);
//	printWidthX24(2, 91, 8, data_0);
//	
//	printWidthX24(4, 67, 8, data_4);
//	printWidthX24(4, 75, 8, data_6);
//	printWidthX24(4, 83, 8, data_0);
//	printWidthX24(4, 91, 8, data_4);
//	printWidthX24(4, 99, 8, data_1);
//	printWidthX24(4, 107, 8, data_2);







	//LCD初始化和打印标题

//	printWidthX24(0,90,21, data_ya);
//	printWidthX24(0,48,14,data_V);
//	
//	//打出“转速   R”
//	printWidthX24(3, 67, 21, data_zhuan);
//	printWidthX24(3,90,21, data_su);
//	printWidthX24(3,48,14,data_R);
//	
//	//打出“按键   ”
//	printWidthX16(6, 67, 16, data_kai);
//	printWidthX16(6,90,16, data_guan);
}
 
////LCD_KEY任务
//void LCD_KEY_task(void *pdata)
//{
//	INT8U err;  //用于读取key事件的错误码
//		
//	while(1)
//	{
//			OSFlagPend (EventFlags,
//							KEY_FLAG,
//						 OS_FLAG_WAIT_SET_ALL + OS_FLAG_CONSUME,
//						 0x00,
//						 &err);
//	if(err == OS_ERR_NONE)
//	{
//		printWidthX16(6,36,8,data_sm_1);
//	}
//	delay_ms(100);
//	
//	 printWidthX16(6,36,8,data_sm_0);
//	
//	}

//}

//LCD任务
void LCD_task(void *pdata)
{	 	
	uint16_t tmp[4];	//存放打印的单个单个数字
	uint16_t tmp2=0x00;	//存放需要打印的数据
	uint16_t wei; //共三个数位
	
	uint16_t tmp3=0x00;	//存放需要打印的数据
	uint16_t tmp3_wei=0;
	uint16_t tmp3_print[4];//存放需要打印的位
	uint16_t tmp3_chu;//存放tmp3除位数时的数据
	
	uint16_t  i;
	uint16_t position;
	
	while(1)
	{		
//		tmp2=MOTOR_ADC*100; //保留两位小数	
//		tmp3=MOTOR_ADC*60; //显示60倍关系,不留小数	
	
	//打印电压值
	wei=3; //共三个数位
	
	for(i=4; i>=1; i--)
	{
		if( (i-1) == 1) //第二位是小数点 直接赋零
			tmp[1]=0;
		else
		{	
			tmp[i-1] = tmp2 % 10 ;
			tmp2 = tmp2 / 10;
		}
	}
	
	//打印一个位
	for(i=0; i< wei+1; i++ )
	{
		if(i==1)
		{
			position = 12;
			printWidthX24(0,position,11,data_dot);
		}
		else 
		{
			if(i==0)
				position = 0;
			else if(i==2)
				position = 24;	
			else if(i==3)
				position =36;	
				switch( tmp[i] )
				{
					case 0:	printWidthX24(0,position,11,data_0);
									break;
					case 1:	printWidthX24(0,position,11,data_1);
									break;
					case 2:	printWidthX24(0,position,11,data_2);
									break;
					case 3:	printWidthX24(0,position,11,data_3);
									break;
					case 4:	printWidthX24(0,position,11,data_4);
									break;
					case 5:	printWidthX24(0,position,11,data_5);
									break;
					case 6:	printWidthX24(0,position,11,data_6);
									break;
					case 7:	printWidthX24(0,position,11,data_7);
									break;
					case 8:	printWidthX24(0,position,11,data_8);
									break;
					case 9:	printWidthX24(0,position,11,data_9);
									break;
					default:;				
				}
			}
		}
	
			//打印转速
			//判断位数
			tmp3_chu=tmp3;
			for(i=0;tmp3_chu>0;i++)
			{
				tmp3_print[i]=tmp3_chu%10;
				tmp3_chu=tmp3_chu/10;
			}
			tmp3_wei=i+1; //tmp3具有i+1位整数

		//打印一个位
		for(i=0; i< tmp3_wei; i++ )
		{
				if(i==0)
					position = 36;
				else if(i==1)
					position = 24;	
				else if(i==2)
					position = 12;	
				else if(i==3)
					position =0;	
					switch( tmp3_print[i] )
					{
						case 0:	printWidthX24(3,position,11,data_0);
										break;
						case 1:	printWidthX24(3,position,11,data_1);
										break;
						case 2:	printWidthX24(3,position,11,data_2);
										break;
						case 3:	printWidthX24(3,position,11,data_3);
										break;
						case 4:	printWidthX24(3,position,11,data_4);
										break;
						case 5:	printWidthX24(3,position,11,data_5);
										break;
						case 6:	printWidthX24(3,position,11,data_6);
										break;
						case 7:	printWidthX24(3,position,11,data_7);
										break;
						case 8:	printWidthX24(3,position,11,data_8);
										break;
						case 9:	printWidthX24(3,position,11,data_9);
										break;
						default:;				
					}
			}
		
			delay_ms(500);
			
	};
}

//////////////////////////////////////////移植与谭玉萍HAL库//////////////
//*****读忙标志位*********
BitAction bCheckBusy(void)
{
  GPIO_WriteBit(GPIOC, D0_Pin |D1_Pin|D2_Pin|D3_Pin|D4_Pin 
                           |D5_Pin|D6_Pin|D7_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, RW_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, RS_Pin,Bit_RESET);
	GPIO_WriteBit(GPIOC, EN_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, EN_Pin,Bit_RESET);
	return GPIO_ReadInputDataBit(GPIOC,D7_Pin); 
}

//******写数据**********??? *
void vWriteData(uint16_t ucData)
{
	uint16_t i=0;
	BitAction ucData_trans[8];
	//数据转换
	datatransform(ucData, ucData_trans ,8);
	
	//测试是否空闲
//	while( bCheckBusy()){};
  GPIO_WriteBit(GPIOC, D0_Pin |D1_Pin|D2_Pin|D3_Pin|D4_Pin 
                           |D5_Pin|D6_Pin|D7_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, RW_Pin,Bit_RESET);
	GPIO_WriteBit(GPIOC, RS_Pin,Bit_SET);
		
	//写入	
	for(i=0;i<8;i++)
	{
		switch(i)
		{
			case 0:GPIO_WriteBit(GPIOC, D0_Pin,*(ucData_trans+i));
				break;
			case 1:GPIO_WriteBit(GPIOC, D1_Pin,*(ucData_trans+i));
				break;
			case 2:GPIO_WriteBit(GPIOC, D2_Pin,*(ucData_trans+i));
				break;
			case 3:GPIO_WriteBit(GPIOC, D3_Pin,*(ucData_trans+i));
				break;
			case 4:GPIO_WriteBit(GPIOC, D4_Pin,*(ucData_trans+i));
				break;
			case 5:GPIO_WriteBit(GPIOC, D5_Pin,*(ucData_trans+i));
				break;
			case 6:GPIO_WriteBit(GPIOC, D6_Pin,*(ucData_trans+i));
				break;
			case 7:GPIO_WriteBit(GPIOC, D7_Pin,*(ucData_trans+i));
				break;
			default:;
		}
	}
	GPIO_WriteBit(GPIOC, EN_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, EN_Pin,Bit_RESET);
}

//******写指令**** 
void vWriteCMD(uint16_t ucCMD)
{	
	uint16_t  i=0;
	BitAction ucCMD_trans[8];
	//数据转换
	datatransform(ucCMD,ucCMD_trans,8);
	
	//测试是否空闲
//	while( bCheckBusy()){};
  GPIO_WriteBit(GPIOC, D0_Pin |D1_Pin|D2_Pin|D3_Pin|D4_Pin 
                           |D5_Pin|D6_Pin|D7_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, RW_Pin,Bit_RESET);
	GPIO_WriteBit(GPIOC, RS_Pin,Bit_RESET);
		
	//写入	
	for(i=0;i<8;i++)
	{
		switch(i)
		{
			case 0:GPIO_WriteBit(GPIOC, D0_Pin,*(ucCMD_trans+i));
				break;
			case 1:GPIO_WriteBit(GPIOC, D1_Pin,*(ucCMD_trans+i));
				break;
			case 2:GPIO_WriteBit(GPIOC, D2_Pin,*(ucCMD_trans+i));
				break;
			case 3:GPIO_WriteBit(GPIOC, D3_Pin,*(ucCMD_trans+i));
				break;
			case 4:GPIO_WriteBit(GPIOC, D4_Pin,*(ucCMD_trans+i));
				break;
			case 5:GPIO_WriteBit(GPIOC, D5_Pin,*(ucCMD_trans+i));
				break;
			case 6:GPIO_WriteBit(GPIOC, D6_Pin,*(ucCMD_trans+i));
				break;
			case 7:GPIO_WriteBit(GPIOC, D7_Pin,*(ucCMD_trans+i));
				break;
			default:;
		}
	}
	GPIO_WriteBit(GPIOC, EN_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, EN_Pin,Bit_RESET);
}

//****LCD初始化函数********?? *
void vLCDInitialize(void)
{
//  GPIO_WriteBit(GPIOC, CS1_Pin| CS2_Pin|RST_Pin,Bit_SET);
	GPIO_WriteBit(GPIOC, RST_Pin,Bit_SET);
	vWriteCMD(0x38);	//8位形式，两行字符。
	
	vWriteCMD(0x0F);	//开显示。
	vWriteCMD(0x01);	//清屏。
	vWriteCMD(0x06);	//画面不动，光标右移。
	vWriteCMD(LCDSTARTROW); //设置起始行。
}

//****显示自定义行*****
//在8×128的格子里显示自定义长度的一行
void vShowCustomRow(uint16_t ucPage,uint16_t ucLine,uint16_t ucWidth,unsigned char *ucaRow)
{
		uint16_t ucCount;	//取值范围：ucPage:0～7；ucLine:0～127；
											//ucWidth:0～127；ucLine+ucWidth<1128。

		if(ucLine<64)
		{
			GPIO_WriteBit(GPIOC, CS1_Pin,Bit_SET);
			GPIO_WriteBit(GPIOC, CS2_Pin,Bit_RESET);
			vWriteCMD(LCDPAGE+ucPage);
			vWriteCMD(LCDLINE+ucLine);
			if((ucLine+ucWidth)<64)
			{
				for(ucCount=0;ucCount<ucWidth;ucCount++)
					vWriteData(*(ucaRow+ucCount));
			}
			else
			{
				for(ucCount=0;ucCount<64-ucLine;ucCount++)
					vWriteData(*(ucaRow+ucCount));
				GPIO_WriteBit(GPIOC, CS1_Pin,Bit_RESET);
				GPIO_WriteBit(GPIOC, CS2_Pin,Bit_SET);
				vWriteCMD(LCDPAGE+ucPage);
				vWriteCMD(LCDLINE);
				for(ucCount=64-ucLine;ucCount<ucWidth;ucCount++)
					vWriteData(*(ucaRow+ucCount));
			}
		}
		else
		{
			GPIO_WriteBit(GPIOC, CS1_Pin,Bit_RESET);
			GPIO_WriteBit(GPIOC, CS2_Pin,Bit_SET);
			vWriteCMD(LCDPAGE+ucPage);
			vWriteCMD(LCDLINE+ucLine-64);
			for(ucCount=0;ucCount<ucWidth;ucCount++)
				vWriteData(*(ucaRow+ucCount));
		}
}
//*******汉字显示函数******
//此函数将16×16汉字显示在8×128的格子里。
void vShowOneChin(uint16_t  ucPage,uint16_t ucLine,unsigned  char *ucaChinMap)
{
	vShowCustomRow(ucPage,ucLine,16,ucaChinMap);
	vShowCustomRow(ucPage+1,ucLine,16,ucaChinMap+16);
}

//**********字符显示函数********* *
//此函数将8×16字符显示在8×128的格子里。
void vShowOneChar(uint16_t ucPage,uint16_t  ucLine, unsigned  char *ucaCharMap)
{
	vShowCustomRow(ucPage,ucLine,8,ucaCharMap);
	vShowCustomRow(ucPage+1,ucLine,8,ucaCharMap+8);
}


//*****显示图片******* 
void vShowGraph(uint16_t  ucPage,uint16_t  ucLine,uint16_t  ucWidth,uint16_t  ucHigh,unsigned  char * ucaGraph)
{
	uint16_t  ucCount;
	for(ucCount=0;ucCount<ucHigh;ucCount++)
		vShowCustomRow(ucPage+ucCount,ucLine,ucWidth,ucaGraph+ucCount*ucWidth);
}
//*******IO数据转换函数*******
void datatransform(uint16_t data, BitAction *transform, uint16_t  length)
{
	uint16_t i=0;
	for(i=0;i<length;i++)
	{
		switch( (data>>i)&0x01 )
		{
			case 1:
			{*(transform+i)=Bit_SET;break;}
			case 0:
			{*(transform+i)=Bit_RESET;break;}	
			default:;			
		}
	}
}
//*******打印24高的字符一个 *******
//startpage ： 0-5 
void printWidthX24(uint16_t startpage, uint16_t startline, uint16_t width, unsigned  char *data)
{
		vShowCustomRow(startpage,startline,width,data);
		vShowCustomRow(startpage+1,startline,width,data+width);
		vShowCustomRow(startpage+2,startline,width,data+width*2);
}

void printWidthX16(uint16_t startpage, uint16_t startline, uint16_t width, unsigned  char *data)
{
		vShowCustomRow(startpage,startline,width,data);
		vShowCustomRow(startpage+1,startline,width,data+width);
}
