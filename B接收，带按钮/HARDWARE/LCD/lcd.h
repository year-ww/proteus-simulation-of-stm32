#ifndef __LCD_H
#define __LCD_H	 
#include "sys.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//架构@正点原子
//修改 湖南大学 谭玉萍
//2021.01
////////////////////////////////////////////////////////////////////////////////// 
#define D7_Pin GPIO_Pin_13
#define RST_Pin GPIO_Pin_0
#define CS1_Pin GPIO_Pin_1
#define CS2_Pin GPIO_Pin_2
#define EN_Pin GPIO_Pin_3
#define RW_Pin GPIO_Pin_4
#define RS_Pin GPIO_Pin_5
#define D0_Pin GPIO_Pin_6
#define D1_Pin GPIO_Pin_7
#define D2_Pin GPIO_Pin_8
#define D3_Pin GPIO_Pin_9
#define D4_Pin GPIO_Pin_10
#define D5_Pin GPIO_Pin_11
#define D6_Pin GPIO_Pin_12

#define LCDSTARTROW 0xC0	//设置起始行指令。
#define LCDPAGE 0xB8	//设置页指令。
#define LCDLINE 0x40	//设置列指令。

extern void LCD_show_number1(void);
extern void LCD_show_number2(void);

extern unsigned  char data_clear[128];
extern unsigned  char data_huang[];
extern unsigned  char data_wang[];

extern unsigned  char data_zi[];
extern unsigned  char data_gjj[];
extern unsigned  char data_guo[];
extern unsigned  char data_dian[];
extern unsigned char data_ya[];
extern unsigned  char data_zhuan[];
extern unsigned char data_su[];
extern unsigned char data_kai[];
extern unsigned  char data_guan[];
extern unsigned char data_sm_1[];
extern unsigned char data_sm_0[];
extern unsigned char data_R[];
extern unsigned  char data_V[];
extern unsigned char data_1[];	
extern unsigned char data_2[];	
extern unsigned char data_3[];
extern unsigned char data_4[];	
extern unsigned char data_5[];
extern unsigned char data_6[];
extern unsigned char data_7[];
extern unsigned char data_8[];
extern unsigned char data_9[];
extern unsigned char data_0[];	
extern unsigned char data_dot[];

extern uint16_t  MOTOR_SPEED;  //共享资源区
extern float  MOTOR_ADC;  //共享资源区





void LCD_Init(void);//初始化
void LCD_task(void *pdata);
void LCD_KEY_task(void *pdata);

//*******数据转换函数*******
void datatransform(uint16_t data, BitAction *transform, uint16_t  length);
 //*****读忙标志位*********
BitAction bCheckBusy(void); 
 //******写数据**********??
void vWriteData(uint16_t ucData);
//******写指令**** 
void vWriteCMD(uint16_t ucCMD); 
//****LCD初始化函数********?? *
void vLCDInitialize(void);
//****显示自定义行*****
//在8×128的格子里显示自定义长度的一行
void vShowCustomRow(uint16_t ucPage,uint16_t ucLine,uint16_t ucWidth,unsigned  char*ucaRow);
//*******汉字显示函数******
//此函数将16×16汉字显示在8×128的格子里。
void vShowOneChin(uint16_t  ucPage,uint16_t ucLine,unsigned  char *ucaChinMap);
//**********字符显示函数********* *
//此函数将8×16字符显示在8×128的格子里。
void vShowOneChar(uint16_t ucPage,uint16_t  ucLine,unsigned  char *ucaCharMap);
//*****显示图片******* 
void vShowGraph(uint16_t  ucPage,uint16_t  ucLine,uint16_t  ucWidth, uint16_t ucHigh,unsigned  char* ucaGraph);
//*******打印24高的字符一个 *******
//startpage ： 0-5 
void printWidthX24(uint16_t startpage, uint16_t startline, uint16_t width, unsigned  char *data);
//*******打印16高的字符一个 *******
void printWidthX16(uint16_t startpage, uint16_t startline, uint16_t width, unsigned  char *data);

//#define RST P2_0?? /*复位信号，低电平复位*/
//#define E P2_1? /*E 使能信号，为H或1时表示DRAM数据读到DB7~DB0，为L或0表示锁存DB7~DB0*/
//#define RW P2_2 /* RW 为1? E 为1时数据被读到DB7~DB0，为0 且E为下降沿时DB7~DB0的数据被写入到IR（指令寄存器）或DR（数据寄存器）*/
//#define DI P2_3? /* DI为1表示DB7~DB0为显示数据，为0表示DB7~DB0为显示指令数据*/
//#define CS1 P2_5?? /*CS1为1表示选择该芯片（右半屏）信号*/
//#define CS2 P2_4?? /*CS2为1表示选择该芯片（左半屏）信号*/

#endif
