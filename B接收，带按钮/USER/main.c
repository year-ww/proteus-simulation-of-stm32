#include "led.h"
#include "delay.h"
#include "sys.h"
#include "key.h"
#include "usart.h"
#include "exti.h" 
#include "lcd.h"

u8 key=0;

void EXTI9_5_IRQHandler(void)
{			
	delay_ms(10);   //消抖	

	if(KEY0==0)	{
		key = 0;
	}
 	 EXTI_ClearITPendingBit(EXTI_Line5);    //清除LINE5上的中断标志位  
}


u8 count = 0;

 int main(void)
 {	

	 
	u16 t ;
	u16 len ;	 
	 
	 
	uint8_t xuehao[10] = {'2', '1', '0', '0', '4', '6', '0', '4', '1', '2'};
	u8 liang[2] = {'1',' '};
	u8 mie[2] = {'2',' '};
	
	delay_init();	    	 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(9600);
	EXTIX_Init();
	LED_Init();	
	LCD_Init();
	
	while(1)
	{
		if(USART_RX_STA)
		{
			len=USART_RX_STA&0x3fff;                 //得到此次接收到的数据长度
			for(t=0;t<len;t++)
			{
				USART_SendData(USART1, USART_RX_BUF[t]);      //向串口1发送数据
				while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET); //等待发送结束
			}
			
			USART_RX_STA = 0 ;
			
			for (t = 0; t < 2; t++)
			{
					if (USART_RX_BUF[t] == liang[t])
					{
							key += 1;
							
					}
					else if(USART_RX_BUF[t] == mie[t])
					{
							key += 3;
							
					}
			}
			for(t = 0; t < 10; t++)
			{
					if (USART_RX_BUF[t] == xuehao[t])
					{
							count++;							
					}						
			}
			
			if (key == 2)
			{
				LED0=0;
				key = 0 ;
			}
		else if(key == 6)
		{
			LED0 = 1 ;
			key = 0 ;
		}		
		if(count==10)
		{
							 
			LCD_show_number1();
		}
		}

	}
	
		
}
